# Deep Homology?

## Uncanny Similarities of Humans and Flies Uncovered by Evo-Devo

## by Lewis Held

This might be the best science book I've ever read!  It shows a clear
"big picture", and also masses of concrete detail.  And it's not very
long.  It's about how similar molecular mechanisms construct many
aspects of animals that seem very different, like humans and flies
(but not only).

The downside is it's a hard book, at least in parts where the reader
needs a lot of molecular biology background.  Some figures had
too-small details and text.  The writing style felt overly fancy at
times.  I think this field is advancing rapidly so the book will get
dated, but I found it very useful 6 years after publication.  Lewis
Held would be great at updating it, I hope he does!

Half of the book is a list of 2560 references.  It could have used
smaller text, to save some bulk.  I can't imagine I'll ever read and
know that much!
