# Thinking like a phage

## by Merry Youle

This is a strong contender for best science book I've ever read.  It's
an advanced textbook disguised as popular science.  It succeeds
amazingly at both.  (A few parts are a bit detailed or heavy for
popular science.)  It's by far the best-written science book I've
seen: this is how we should write!

It's about phages: viruses that infect bacteria and archaea.  Phages
are arguably the most important, diverse, and numerous biological
entities.  The book details each step of the phage life cycle, in
diverse phages.  It superbly conveys the big picture, rather than
drowning in details.  It taught me a few of the most important
"numbers", like speed and error rates of DNA replication and
transcription, and relative strength of molecular motors.

The book invents cute names for phages, which is great because the
standard names are impossible-to-remember things like T4 or phiX174.
Good names are important!

There were just a couple of inadequate explanations.  How can bacteria
acquire new restriction enzymes without killing themselves?  Energy
makes proton motive force, which makes energy?  There are a few UAs
(unhelpful acronyms): I would say "outer membrane" rather than "OM".
There was a tiny bit of unexplained silly jargon, like "in vitro".

There is mention of a planned book "Phage in Community", which seems
yet more interesting: I'm sad it doesn't seem to exist.  There's also
an advert for a "build your own virion" scale model - I wanted to
order one! - but unfortunately it's just a joke?
