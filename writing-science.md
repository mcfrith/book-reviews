# Writing Science

## By Joshua Schimel

Writing is an important part of science: it's not number 1, but if you
write really nicely and clearly, people will read and understand what
you did!  So it's worth reading several books on writing.  This book
is now my top recommendation: it's amazing!

His main advice is to tell a good story.  But it's important to find
the story in the data, not force the data into a story.  There are
four core story structures (in general writing, not specific to
science).  These structures should also be used for sub-story "arcs",
paragraphs, and even sentences.  Sentences have "stress" and "topic"
positions, which can be connected to give *flow*.

The book has many samples of good and bad writing, with worked
examples of improving them.  I occasionally disagree with what's good
and bad.  There is well-balanced discussion of good/bad, e.g. active
is usually better than passive, but not always.  I think a lot can be
summarized as: vague is bad.

The book's number one rule is: make the reader's job easy.  One
interesting consequence is that it's not always sufficient to cite
something: we should also summarize the thing we're citing if it's
important for our story.

There is good news for non-native English speakers: the most important
thing is to structure a good story, which is language-independent.
Non-native speakers can compete in this aspect!

The book is physically small and light, easy to carry around.

There is one glaring omission: the book says nothing about titles or
abstracts.  Which are the only parts that most people read.  In
particular, I want to know how an abstract fits into the story
"opening".

I don't like the book's subtitle: "how to write papers that get cited
and proposals that get funded".  Our real aim should be to do great
science and communicate it effectively.

Occasionally he's uncharacteristically dogmatic: "The resolution
*must* close the circle".  "Plot twists are forbidden".  Once in my
life, I want to write a scientific paper with an awesome plot twist!
