# The Art of Writing Efficient Programs

## by Fedor Picus

This is an excellent, clear book on optimizing software to run fast.
It's not for beginners: it assumes some background experience with a
compiled language like C or C++.  The examples are all in C++, even
though most of the points aren't specific to that language, so you
need to be reasonably comfortable with C++.

My favorite part was the explanation of how modern CPUs optimize with
instruction-level parallelism, pipelining, and speculative execution.
I vaguely knew this before, but the book really clarified it for me!
I finally get that the CPU optimizes how it runs machine code,
separately from the compiler generating optimized machine code.  I
love that the book (with appropriate caution) gives useful specific
numbers, like L1 cache most commonly being 32 KB, and a cache line 64
bytes.  The explanation of false sharing and cache lines was much
clearer than the confusing stuff I've seen before.  As a "practical
application" of all this, the book even explains the Spectre /
Meltdown security vulnerability.

There's mostly-excellent explanation of concurrency/threads with
locks, atomic variables, and memory order.  It's great that the book
includes lots of real profiling results, which show that real
performance isn't always straightforward.  There's a wonderfully clear
explanation of coroutines (though the relevance to performance is
unclear, and C++ seems to have doubled down on being a disgusting
language in its coroutine features).

There's good advice on helping the compiler to optimize your code,
including by exploiting the compiler's disregard for "undefined
behavior".  I'm surprised that SIMD isn't included.

The tone is informal, friendly, and clear: mostly great, but
occasionally a bit verbose.

Weak points:

* Requires the reader to know more C++ than necessary.

* I think the book's lock-free stack isn't lock-free.  It uses 2
  integers: when they're equal it's unlocked, when they're unequal
  it's locked.  It's totally a lock.  Isn't it?  I'm not clear on what
  "lock-free" really means...

* There's a useful early chapter on profiling tools like `perf` and
  `gperftools`, but it's a bit too brief for me to use them
  effectively.  And when I tried them (on Ubuntu, which I think is
  pretty mainstream), it was frustrating: it wasn't clear what to
  install, and then `perf` had some `perf_event_paranoid` problem, and
  `gperftools` did nothing (without secret-voodoo compiler flags:
  `-Wl,--no-as-needed` or something).

* I think there are some typos, and some of the figures look like
  they've been converted from color to grayscale making them unclear.
