# Show Me the Numbers

## by Stephen Few

This book is an excellent guide to designing graphs and tables.  As
always, don't automatically believe it all: the main benefit I got is
it made me think.

My student made me realize that we don't teach how to graph data, even
though that's a major part of scientific reports.  Actually,
visualizations made by professional scientific researchers are often
atrocious (and I now think mine weren't so great either), so this
should be taught!

The book has interesting background on visual perception and Gestalt
psychology, a solid basis for its detailed advice on displaying data.
Its examples are mostly "business" things (e.g. marketing department
revenues), a shame because I think the world's scientists and
engineers are non-negligible data visualizers too.  So is this book
relevant to science as well as business?  I think mostly "yes".  In
both cases, the point is to convey your message clearly and honestly.
In both cases, fancy-but-confusing graphics are often regarded as
good, but are actually bad.  A scientific audience often has diverse
backgrounds, often without much statistical training.

I found the book too long and verbose, too much "telling".  It's
physically big too, so inconvenient to read in a cafe or on a train.

It recommends some other books - these ones stand out.

* "The Visual Display of Quantitative Information" by Edward Tufte: I
  skim-read it long ago and liked it a lot.
* "The Elements of Graphing Data" by William S. Cleveland: I read some
  pages on the Web, and it looks excellent!

**Update:** There's a superb introduction to graph design in chapter 1
of [Data Visualization][] by Kieran Healy.

[Data Visualization]: https://socviz.co/
