# The Skeptics' Guide to the Universe

## How to Know What's Really Real in a World Increasingly Full of Fake

## by Steven Novella and friends

In short, everyone should read this book!  But don't automatically
believe it all: apply the book's skepticism to itself.

Since becoming a "professor" I've thought about what's the most
important thing for students to learn, and my answer is "critical
thinking", by which I mean rational thinking, based on reasoning and
evidence, instead of just believing what you hear.  It might be the
most important thing for humankind.  So I thought I'd better read this
book.

It's a really excellent book, even though I only agree with, say, 85%
of it.  It's written in a friendly way for non-scientists, though much
of the content is very sophisticated.  It starts with a surprisingly
large amount of neuropsychology, how our brains imperfectly infer the
world: as far as I know, this is a good summary of current scientific
understanding.  (The main author seems to be a neurologist who knows
his stuff.)  The book then surveys classic fallacies like "argument
from authority", "ad hominem", "genetic fallacy" (do you know what
that is?), and many others.  It considers gray areas where these
fallacies are not so clear-cut.  It covers rhetorical techniques that
are used for anti-rational and biased thinking.  It even touches on
philosophy of science (e.g. Kuhn, demarcation problem).  It's
extremely useful to learn about these key ideas, even (especially?) if
you don't completely agree with the book.

In my opinion, the book tends to be a little too unquestioning of
"mainstream science" and dismissive of the alternatives.  So I was
glad it covers problems in current scientific research: "much of what
passes for scientific research is flawed or preliminary.  Some of it
is utter crap."

The book then covers some instructive historical problems in science:
"Clever Hans" (really interesting), N-rays, etc.  The "Hawthorne
effect" is another case where it doesn't stop at the simple version,
but honestly investigates the more complex full story.

There's a chapter on Genetically Modified Organisms: even though I
don't completely agree with it, I found its investigations of the
various issues really valuable.  I loved this line on escaped GMOs:
"Crops out in the wild would have about as much chance as a dachshund
on the Serengeti."  (But what about cross-pollination with wild plants
mentioned one page before?)

The book also covers problems with science journalism and social
media, and fraudsters who want to sell you something.  I was surprised
(and impressed) that they criticise "positive thinking" (up to a
point), and argue that pessimism can sometimes be good!

The Epilogue sums it up: "Don't trust us... think for yourself."  I
learned a lot from this book, and it's made me a better skeptic.
