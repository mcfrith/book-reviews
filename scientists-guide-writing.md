# The Scientist's Guide to Writing

## By Stephen B. Heard

This is an excellent book, mostly on how to write scientific papers,
but also with bits on peer review, co-authorship, citation, choosing a
journal, how to read lots of papers, etc.  I disagree with a few
things, but overall it's wise advice for academic scientists at an
early (or late) career stage.

The main point is to make writing **crystal clear** like
**telepathy**.  The next thing is to stick to a story and ruthlessly
omit anything unnecessary.

The book has much on psychology and motivation.  Yes I know my problem
is wasting time reading websites... but he suggests specific apps to
help with that, and it's making a difference for me!

A small part of the book is on paragraphs, sentences, and words: this
taught me some important basics of writing!  I share his feeling that
contractions (don't, it's) are good, and pedantic grammar rules are
wrong.  But I can't explain why some rules seem right (e.g. don't mix
up "its" and "it's").

He suggests that method descriptions might not need enough detail to
be reproducible.  I think that's dangerous, because, often, a small
detail of the methods can make a study completely invalid.

There's a chapter on citation, which I mostly agree with.  Yes, the
main point of citation is to support claims, and be useful to the
reader (which favors easy-to-access sources).  Primary sources should
be favored over secondary, and earlier over later.  Citation bias
against groups of people is indeed a problem, but the converse
rich-get-richer "Matthew Effect" should also be mentioned.

The book has some disturbing suggestions of establishing "authority"
by "standard" methods, citations, and affiliations.  That reeks of
anti-scientific appeal-to-authority and cargo-culting, though I
concede it may be what scientists actually do.

The book rightly says that writing overlaps with doing the science,
because while writing we realise what analyses are needed.  But it
goes too far in suggesting early writing with mock tables and figures.
Research should be partly about exploring the unknown with an open
mind: mock results are surely a psychological barrier to that.  And
they increase the risk of fake results getting published - surely that
wouldn't happen? - but it does.

I felt the book was a bit too respectful of the currently-standard
paper structure (and of peer review).  Writing should presumably start
with an introduction and end with some sort of conclusion, but the
rest is historical accident.  (Putting method details in an appendix
seems good, though.)

Overall, a thoughtful and impressively-researched book with easy-going
humor.  A much shorter version of it would be great for busy people.
