# Understanding Evo-Devo

## by Wallace Arthur

This small book is a wonderfully clear introduction to evo-devo
(evolutionary developmental biology).  I now feel I understand the
main kinds of genes and processes involved.  It has fascinating
examples like turtle shells, and centipedes that only have odd numbers
of segments.

Much of the book is on the history and "philosophy" of the subject.  I
suspect the "philosophy" is partly important, partly vacuous.  Some of
the history is valuable, e.g. I've vaguely heard "development
recapitulates evolution", but it turns out people have said subtly
different things about that, and the book carefully checks what they
actually said.  (My take-home message is, yet again, never completely
trust what you're told.)

The book is heavy on "evo", light on "devo".  For example, it has a
section on notch signalling in segment development, which says almost
nothing about how notch signalling contributes to segment development.
If the key discovery is that different animals have similar
developmental genes, I'd like more details on what these genes are and
what they do.
