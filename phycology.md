# Phycology (5th edition)

## By Robert Edward Lee

This is a textbook on algae: cyanobacteria, plus eukaryotes that
gained plastid organelles related to cyanobacteria.  The book excludes
land plants, which are one sub-sub type of algae.  It covers a wide
range of life, because distantly-related kinds of eukaryote gained
plastids.  It has lots of hand drawings: cute and mostly good.

I learned about weird and wonderful algae, including neglected kinds
of multicellular life.  They often have alternation of generations:
their life cycle alternates between two different-looking forms, often
mistaken for unrelated organisms.  *Volvox* is a multicelled spinning
globe, with separate body cells and reproductive cells, and embryos
that turn inside-out, like gastrulation in animals.  The water net is
a multicellular net, which makes a baby net inside one parent cell.
Then there are giant single cells, like *Acetabularia* (mermaid's
wineglass), 10 cm with just one cell and one nucleus hiding in its
rootlet.

The book's main problem is: too much detail!  It covers so many
sub-sub-sub types of algae, which often seem similar to each other.
It has hundreds of chemical diagrams.  It's hard to pick out the main
things to learn.  It assumes the reader already knows details of
photosynthesis (which of course I've mostly forgotten): a book on
algae should surely cover photosynthesis.  Some difficult jargon isn't
introduced (though at least there's a good glossary): who knew that
"sessile" can mean "lacking a stalk"?
