# The Fungi (3rd edition)

## by Sarah Watkinson, Lynne Boddy, Nicholas Money

After reading this I feel like I "get" fungi, and they're amazing, I
think they're the ultimate life form!  They grow into their food, and
some can grow long distances towards food.  They connect to plant
roots and exchange nutrients with them, forming a "wood-wide web".  If
they run out of food, they shed billions of spores, some of which will
find more food.  Cannot be stopped!

It seems every genetic phenomenon I could imagine, and some I
couldn't, happens in fungi.  Male/female is laughably primitive: fungi
have any number of mating types.  Even the idea of "individual" is
tricky, because fungi can break into parts and fuse together.

I was hugely impressed by the authors' expertise in general molecular
biology, which gave me some impostor syndrome.  On the downside, the
figures are poor: a biology book like this needs great figures.  Some
of the writing is bad, with non-cohesive paragraphs, and some is too
terse.  It's a difficult book: the reader needs to know a lot of
molecular biology.
